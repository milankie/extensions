import sys
from unittest import TestCase
from inkex.paths import Path
from inkex.boolean_operations import BooleanOperator, combine_segments
from svgpathtools.path import Path as PTPath
import svgwrite

from traceback import extract_stack


class PathOffset(TestCase):

    def test_offset_square(self):
        rectangle = Path("M 0,0 L 0,2 L 2,2 L 2,0 z")
        rectangle_offset = rectangle.offset(1)
        print(str(rectangle_offset))
        assert str(rectangle_offset) == "M 0 -1 A 1 1 0 0 0 -1 0 L -1 2 A 1 1 0 0 0 0 3 L 2 3 A 1 1 0 0 0 3 2 L 3 0 A 1 1 0 0 0 2 -1 z"

    def test_offset_circle(self):
        circle = Path("M 50,0 A 50,50 0 0 0 0,50 50,50 0 0 0 50,100 50,50 0 0 0 100,50 50,50 0 0 0 50,0 Z")
        circle_offset = circle.offset(1)
        assert str(circle_offset) == "M 50 -1 A 51 51 0 0 0 -1 50 A 51 51 0 0 0 50 101 A 51 51 0 0 0 101 50 z"

    def test_offset_donut(self):
        donut = Path("M 50,0 A 50,50 0 0 0 0,50 50,50 0 0 0 50,100 50,50 0 0 0 100,50 50,50 0 0 0 50,0 Z m 0,25 A 25,25 0 0 1 75,50 25,25 0 0 1 50,75 25,25 0 0 1 25,50 25,25 0 0 1 50,25 Z")
        donut_offset = donut.offset(1)
        print(str(donut_offset))
        assert str(donut_offset) == "M 50,-1 A 51,51 0 0 0 -1,50 51,51 0 0 0 50,101 51,51 0 0 0 101,50 51,51 0 0 0 50,-1 Z m 0,27.207456 A 23.792543,23.792542 0 0 1 73.79255,50 23.792543,23.792542 0 0 1 50,73.79255 23.792543,23.792542 0 0 1 26.207456,50 23.792543,23.792542 0 0 1 50,26.207456 Z"
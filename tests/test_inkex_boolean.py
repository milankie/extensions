import pytest
from unittest import TestCase
from inkex.paths import Path
from inkex.tester.geometry_utils import sample_area_offset
from inkex.boolean_operations import BooleanOperator, combine_segments
from svgpathtools.path import Path as PTPath
import svgwrite
import os

FOLDERNAME = os.path.dirname(__file__)

from traceback import extract_stack


def compare_paths(result: Path, target: Path):
    calling_func = extract_stack(limit=2)[0]
    complete_bbox = result.bounding_box() + target.bounding_box()
    result_d = str(result)
    target_d = str(target)
    if result_d != target_d:
        dwg = svgwrite.Drawing(f"output/{calling_func.name}.svg", profile="full")
        dwg.viewbox(
            complete_bbox.left,
            complete_bbox.top,
            complete_bbox.width,
            complete_bbox.height,
        )
        if result_d:
            dwg.add(
                dwg.path(
                    d=result_d,
                    fill=svgwrite.rgb(255, 0, 0, "%"),
                    stroke="none",
                    fill_opacity=0.1,
                    id=f"result_d",
                )
            )
        if target_d:
            dwg.add(
                dwg.path(
                    d=target_d,
                    fill=svgwrite.rgb(0, 255, 0, "%"),
                    stroke="none",
                    fill_opacity=0.1,
                    id=f"target_d",
                )
            )
        dwg.save()

    assert (
        result_d == target_d
    ), f"paths don't match: result_d - {result_d} target_d - {target_d}"


def plot_graph(bool_op):
    calling_func = extract_stack(limit=2)[0]
    dwg = svgwrite.Drawing(f"output/{calling_func.name}_graph.svg", profile="full")
    min_x = None
    max_x = None
    min_y = None
    max_y = None
    for i, edge in enumerate(bool_op.edges):
        _min_x, _max_x, _min_y, _max_y = edge.bbox()
        if not min_x:
            min_x, max_x, min_y, max_y = edge.bbox()
        min_x = min(_min_x, min_x)
        min_y = min(_min_y, min_y)
        max_x = max(_max_x, max_x)
        max_y = max(_max_y, max_y)

        dwg.add(
            dwg.path(
                d=edge.d(),
                stroke_width=0.01,
                stroke=svgwrite.rgb(255, 0, 0, "%"),
                fill="none",
                id=f"path{i}",
            )
        )
        if i in bool_op.graph:
            dwg.add(
                dwg.path(
                    d=edge.d(),
                    stroke_width=0.01,
                    stroke=svgwrite.rgb(0, 255, 0, "%"),
                    fill="none",
                    id=f"path_after_{i}",
                )
            )
    dwg.viewbox(min_x, min_y, max_x - min_x, max_y - min_y)
    dwg.save()


class PathBoolean(TestCase):
    def setUp(self):
        self.rectangle_bigger = "M 0,0 L 0,2 L 2,2 L 2,0 z"
        self.rectangle_smaller = "M 0.5,0.5 L 0.5,1.5 L 1.5,1.5 L 1.5,0.5 z"
        rectangle_outside = "M 0,1.5 L 0.5,1.5 L 0.5,2.5 L 0,2.5 z"
        rectangle_outside_union = "M 0.5 2 L 0.5 2.5 L 0 2.5 L 0 2 L 0.5 2 L 2 2 L 2 0 L 0 0 L 0 1.5 M 0 2 L 0 1.5 L 0 2"
        self.pvRectangleBigger = Path(self.rectangle_bigger)
        self.pvRectangleSmaller = Path(self.rectangle_smaller)
        self.pvRectangleOutside = Path(rectangle_outside)
        self.pvTargetUnion = Path(rectangle_outside_union)
        self.pvEmpty = Path("")
        self.pv_with_island = Path().from_svgpathtools(
            combine_segments(
                [segment for segment in self.pvRectangleBigger.to_svgpathtools()]
                + [
                    segment
                    for segment in self.pvRectangleSmaller.to_svgpathtools().reversed()
                ]
            )
        )

    def test_convert_path_to_from(self):
        _pt_path = PTPath(self.rectangle_bigger)
        _inkex_path = Path().from_svgpathtools(_pt_path)
        _out_pt_path = _inkex_path.to_svgpathtools()
        assert _pt_path.d() == _out_pt_path.d()

    def test_convert_path_from_to(self):
        print(self.pvRectangleSmaller)
        _pt_path = self.pvRectangleSmaller.to_svgpathtools()

        assert (
            _pt_path.isclosed()
        ), f"start does not equal end! {_pt_path.start=} {_pt_path.end=}"
        assert _pt_path.iscontinuous()
        _inkex_path = Path().from_svgpathtools(_pt_path)

        assert str(self.pvRectangleSmaller) == str(
            _inkex_path
        ), f"paths are not equal {str(self.pvRectangleSmaller)} {str(_inkex_path)}"

    def test_create_graph(self):
        # confirm that the graph is being created as expected
        bool_op = BooleanOperator(
            self.pvRectangleBigger.to_svgpathtools(),
            self.pvRectangleOutside.to_svgpathtools(),
        )
        assert bool_op.graph
        for i in range(5):
            assert i in bool_op.graph

    def test_prune(self):
        # confirm that pruning removes edges
        bool_op = BooleanOperator(
            self.pvRectangleBigger.to_svgpathtools(),
            self.pvRectangleOutside.to_svgpathtools(),
            debug=True,
        )
        original_keys = list(bool_op.graph.keys())
        bool_op.prune(lambda x: bool_op.is_inside(x, 0) or bool_op.is_inside(x, 1))
        print("graph before", bool_op.graph)
        new_keys = list(bool_op.graph.keys())
        # if len(new_keys) >= len(original_keys):
        dwg = svgwrite.Drawing(f"data/refs/test_prune.svg", profile="full")
        dwg.viewbox(0, 0, 2.5, 2.5)
        for i, edge in enumerate(bool_op.edges):
            dwg.add(
                dwg.path(
                    d=edge.d(),
                    stroke_width=0.01,
                    stroke=svgwrite.rgb(255, 0, 0, "%"),
                    fill="none",
                    id=f"path{i}",
                )
            )
            if i in bool_op.graph:
                dwg.add(
                    dwg.path(
                        d=edge.d(),
                        stroke_width=0.01,
                        stroke=svgwrite.rgb(0, 255, 0, "%"),
                        fill="none",
                        id=f"path_after_{i}",
                    )
                )
        dwg.save()
        print("graph_after", bool_op.graph)
        assert 3 not in new_keys
        assert 1 not in new_keys
        assert 5 not in new_keys  # this is the edge place
        assert len(new_keys) < len(
            original_keys
        ), f"keys have not been pruned: {new_keys} {original_keys}"

    def test_prune_swap(self):
        # confirm that pruning removes edges
        bool_op = BooleanOperator(
            self.pvRectangleOutside.to_svgpathtools(),
            self.pvRectangleBigger.to_svgpathtools(),
            debug=True,
        )
        original_keys = list(bool_op.graph.keys())
        bool_op.prune(lambda x: bool_op.is_inside(x, 0) or bool_op.is_inside(x, 1))
        print("graph before", bool_op.graph)
        new_keys = list(bool_op.graph.keys())
        # if len(new_keys) >= len(original_keys):
        dwg = svgwrite.Drawing(f"data/refs/test_prune_swap.svg", profile="full")
        dwg.viewbox(0, 0, 2.5, 2.5)
        for i, edge in enumerate(bool_op.edges):
            dwg.add(
                dwg.path(
                    d=edge.d(),
                    stroke_width=0.01,
                    stroke=svgwrite.rgb(255, 0, 0, "%"),
                    fill="none",
                    id=f"path{i}",
                )
            )
            if i in bool_op.graph:
                dwg.add(
                    dwg.path(
                        d=edge.d(),
                        stroke_width=0.01,
                        stroke=svgwrite.rgb(0, 255, 0, "%"),
                        fill="none",
                        id=f"path_after_{i}",
                    )
                )
        dwg.save()
        print("graph_after", bool_op.graph)
        assert 0 not in new_keys
        assert 3 not in new_keys  # this is the overlap outside piece
        # assert 5 not in new_keys
        assert len(new_keys) < len(
            original_keys
        ), f"keys have not been pruned: {new_keys} {original_keys}"

    def test_union_empty_pathvector(self):
        # test that the union of two objects where one is empty results in the same shape
        pv_rectangle_union = Path().union(self.pvRectangleBigger)
        compare_paths(pv_rectangle_union, self.pvRectangleBigger)

    def test_union_empty_swap(self):
        # test that the union of two objects where one is empty results in the same shape
        pv_rectangle_union = self.pvRectangleBigger.union(Path())
        compare_paths(pv_rectangle_union, self.pvRectangleBigger)

    def test_union_outside(self):
        # test that the union of two objects where one is outside the other results in a new larger shape
        pv_rectangle_union = self.pvRectangleBigger.union(self.pvRectangleOutside)
        compare_paths(pv_rectangle_union, self.pvTargetUnion)

    def test_union_outside_swap(self):
        # test that the union of two objects where one is outside the other results in a new larger shape, even when
        # the order is reversed
        pv_rectangle_union = self.pvRectangleOutside.union(self.pvRectangleBigger)
        compare_paths(pv_rectangle_union, self.pvTargetUnion)

    def test_union_inside_swap(self):
        # test that the union of two objects where one is completely inside the other is the larger shape, even when
        # the order is swapped
        pv_rectangle_union = self.pvRectangleSmaller.union(self.pvRectangleBigger)
        compare_paths(pv_rectangle_union, self.pvRectangleBigger)

    def test_intersection_inside(self):
        # test that the intersection of two objects where one is completely inside the other is the smaller shape
        pv_rectangle_intersection = self.pvRectangleBigger.intersection(
            self.pvRectangleSmaller
        )
        compare_paths(pv_rectangle_intersection, self.pvRectangleSmaller)

    def test_intersection_same(self):
        # test that the intersection of two objects where one is completely inside the other is the smaller shape
        pv_rectangle_intersection = self.pvRectangleBigger.intersection(
            self.pvRectangleBigger
        )
        compare_paths(pv_rectangle_intersection, self.pvRectangleBigger)

    def test_interesction_inside_3(self):
        smaller = "M 122.407,26.3068 L 131.407,26.3068 L 131.407,26.8068 L 122.407,26.8068 L 122.407,26.3068"
        larger = "M 32.4069,26.3068 L 130.7715,26.3068 L 130.7715,82.3517 L 32.40690000000001,82.3517 L 32.4069,26.3068"
        bool_op = BooleanOperator(PTPath(smaller), PTPath(larger), debug=True)
        original_keys = list(bool_op.graph.keys())
        print(f" graph before: {bool_op.graph}")
        for i, edge in enumerate(bool_op.edges):
            should_prune = bool_op.intersection_should_prune(edge)
            print(f"{i} {should_prune}")
        bool_op.prune(bool_op.intersection_should_prune)
        assert bool_op.is_inside(bool_op.edges[1], 1) == None
        assert bool_op.is_inside(bool_op.edges[2], 1, debug=True) == None
        new_keys = list(bool_op.graph.keys())
        print(f" graph after: {bool_op.graph}")
        plot_graph(bool_op)
        assert new_keys

    def test_intersection_inside_4(self):
        smaller = (
            "M 127.407 81.3068 L 127.907 81.3068 L 127.907 90.3068 L 127.407 90.3068 Z"
        )
        larger = "M 32.4069 26.3068 h 98.3646 v 56.0449 h -98.3646 z"
        bool_op = BooleanOperator(PTPath(smaller), PTPath(larger), debug=True)
        original_keys = list(bool_op.graph.keys())
        print(f" graph before: {bool_op.graph}")
        bool_op.prune(bool_op.intersection_should_prune)
        print(f" graph after: {bool_op.graph}")

        assert not bool_op.is_inside(bool_op.edges[3], 0, debug=True)
        assert bool_op.is_inside(bool_op.edges[3], 1) is None
        print(bool_op.is_inside(bool_op.edges[2], 0, debug=True))
        assert bool_op.is_inside(bool_op.edges[2], 0)
        assert bool_op.is_inside(bool_op.edges[2], 1) is None

        for i, edge in enumerate(bool_op.edges):
            should_prune = bool_op.intersection_should_prune(edge)
            print(f"{i} {should_prune}")

        new_keys = list(bool_op.graph.keys())
        assert 3 not in bool_op.graph
        assert 2 in bool_op.graph
        plot_graph(bool_op)
        output = Path().from_svgpathtools(bool_op.chain_graph())
        target = (
            "M 127.407 21.3068 L 127.907 21.3068 L 127.907 25.3068 L 127.407 25.3068 Z"
        )
        # compare_paths(output, Path(target))

    def test_intersection_inside_2(self):
        smaller = (
            "M 127.407 21.3068 L 127.907 21.3068 L 127.907 30.3068 L 127.407 30.3068 Z"
        )
        larger = "M 32.4069 26.3068 h 98.3646 v 56.0449 h -98.3646 z"
        bool_op = BooleanOperator(PTPath(smaller), PTPath(larger), debug=True)
        original_keys = list(bool_op.graph.keys())
        print(f" graph before: {bool_op.graph}")
        bool_op.prune(bool_op.intersection_should_prune)
        assert not bool_op.is_inside(bool_op.edges[3], 0)
        # assert not bool_op.is_inside(bool_op.edges[3], 1)
        new_keys = list(bool_op.graph.keys())

        print(f" graph after: {bool_op.graph}")
        # if len(new_keys) >= len(original_keys):
        assert 3 not in new_keys
        plot_graph(bool_op)
        output = Path().from_svgpathtools(bool_op.chain_graph())
        target = "M 127.907 26.3068 L 127.907 30.3068 L 127.407 30.3068 L 127.407 26.3068 L 127.907 26.3068"
        compare_paths(output, Path(target))

    def test_intersection_inside_5(self):
        smaller = (
            "M 32.4069 26.3068 L 32.9069 26.3068 L 32.9069 35.3068 L 32.4069 35.3068 z"
        )
        larger = "M 32.4069 26.3068 h 98.3646 v 56.0449 h -98.3646 z"
        _intersect_pattern = Path(smaller).intersection(Path(larger), debug=True)

        compare_paths(_intersect_pattern, Path(smaller))

    def test_intersection_inside_6(self):
        smaller = (
            "M 32.4069 76.3068 L 32.9069 76.3068 L 32.9069 85.3068 L 32.4069 85.3068 Z"
        )
        larger = "M 32.4069 26.3068 h 98.3646 v 56.0449 h -98.3646 z"
        bool_op = BooleanOperator(PTPath(smaller), PTPath(larger), debug=True)
        original_keys = list(bool_op.graph.keys())
        print(f" graph before: {bool_op.graph}")
        bool_op.prune(bool_op.intersection_should_prune)
        assert not bool_op.is_inside(bool_op.edges[3], 0)
        # assert not bool_op.is_inside(bool_op.edges[3], 1)
        new_keys = list(bool_op.graph.keys())

        print(f" graph after: {bool_op.graph}")
        plot_graph(bool_op)
        output = Path().from_svgpathtools(bool_op.chain_graph())
        target = (
            "M 32.4069 76.3068 L 32.9069 76.3068 L 32.9069 82.3068 L 32.4069 82.3068 Z"
        )
        compare_paths(output, Path(target))

    def test_intersection_path(self):
        # test that intersection works for paths and solid objects
        diagonal_line = "M 0,0 L 3,3 z"
        pvDiagonalLine = Path(diagonal_line)
        smaller_line = "M 0,0 L 2,2 z"
        pvSmallerLine = Path(smaller_line)

        path_intersection = self.pvRectangleBigger.intersection(pvDiagonalLine)
        compare_paths(path_intersection, pvSmallerLine)

    def test_offset(self):
        param_list = [
            ("M 0,0 L 3,3 z", "line"),
            ("M 10 10 C 20 20, 40 20, 50 10", "cubic"),
            ("M300,200 h-150 a150,150 0 1,0 150,-150 z", "arc"),
            (
                "m 105.9676,151.90373 h -1.03629 c -0.86867,-2.86254 0.84507,-10.53371 1.03628,-3.99631 z",
                "complex",
            ),
        ]
        for _line, name in param_list:
            with self.subTest():
                offset_path = Path(_line).offset(3)
                dwg = svgwrite.Drawing(
                    os.path.join(FOLDERNAME, f"data/refs/test_offset_{name}.svg"),
                    profile="full",
                )
                dwg.viewbox(0, 0, 2.5, 2.5)
                dwg.add(
                    dwg.path(
                        d=_line,
                        stroke_width=0.01,
                        stroke=svgwrite.rgb(255, 0, 0, "%"),
                        fill="none",
                        id=f"original_line",
                    )
                )
                dwg.add(
                    dwg.path(
                        d=str(offset_path),
                        stroke_width=0.01,
                        stroke=svgwrite.rgb(0, 255, 0, "%"),
                        fill="none",
                        id=f"offset_path",
                    )
                )
                dwg.save()
                assert offset_path is not None
                sample_area_offset(Path(_line), 3, offset_path)

    def test_intersection_path_outline(self):
        # test that intersection works for paths when there's an outline and solid objects
        diagonal_line = "M 0,0 L 3,3 z"
        pvDiagonalLine = Path(diagonal_line).offset(0.1)
        assert pvDiagonalLine is not None

        smaller_line = "M 0 0 V 0.0703125 L 1.9296875 2 H 2 V 1.9296875 L 0.0703125 0 z"
        pvSmallerLine = Path(smaller_line)
        path_intersection = self.pvRectangleBigger.intersection(pvDiagonalLine)
        compare_paths(path_intersection, pvSmallerLine)

    def test_difference_inside(self):
        # test that the difference of two objects where one is completely inside the other has an inner island
        pv_rectangle_difference = self.pvRectangleBigger.difference(
            self.pvRectangleSmaller
        )
        compare_paths(pv_rectangle_difference, self.pv_with_island)

    def test_difference_outside(self):
        # test that the difference of two objects where one is completely outside the other has an inner island
        pv_rectangle_difference = self.pvRectangleSmaller.difference(
            self.pvRectangleBigger
        )
        compare_paths(pv_rectangle_difference, self.pv_with_island)

    # tests TODO:
    # - shapes with islands
    # - shapes with inner islands (both clockwise and counterclockwise)

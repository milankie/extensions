from hypothesis import given, settings
import hypothesis.strategies as st
from inkex.tester.geometry_utils import sample_area, sample_area_offset, GRID_SIZE
from inkex.paths import Path, line, quadratic, curve, move, Move, zoneClose, arc
from unittest import TestCase

ranged_float = st.integers(-GRID_SIZE, GRID_SIZE) # use integers instead of floats to exclude nan and inf and restrict the search space
pi_range = st.integers(1 , 90)
path_type_def = st.lists(st.one_of(
    st.builds(move, ranged_float, ranged_float),
    st.builds(line, ranged_float, ranged_float),
    st.builds(arc, ranged_float, ranged_float, pi_range, st.booleans(), st.booleans(), ranged_float, ranged_float),
    st.builds(quadratic, ranged_float, ranged_float, ranged_float, ranged_float),
st.builds(curve, ranged_float, ranged_float, ranged_float, ranged_float,ranged_float, ranged_float)))


def gen_rect(x, y, width, height):
    if width > x-GRID_SIZE:
        width = abs(x-GRID_SIZE)
    if height > y-GRID_SIZE:
        height = abs(y-GRID_SIZE)
    return Path([Move(x, y), line(width, 0), line(0, height), line(-width, 0), line(0, -height), zoneClose()])


def gen_beziers(x, y, width, height):
    if width > x-GRID_SIZE:
        width = abs(x-GRID_SIZE)
    if height > y-GRID_SIZE:
        height = abs(y-GRID_SIZE)
    return Path([Move(x, y), quadratic(width, 0, width, height), quadratic(0, -height, -width, -height), zoneClose()])


shape_type_def = st.one_of(
    st.builds(gen_rect, ranged_float, ranged_float, ranged_float, ranged_float),
    st.builds(gen_beziers, ranged_float, ranged_float, ranged_float, ranged_float)
)

DEADLINE = 1600


class PathBooleanHypothesis(TestCase):
    """
    @given(st.tuples(path_type_def, path_type_def))
    def test_union(self, paths):
        Path(paths[0]).union(Path(paths[1]))

    @given(st.tuples(path_type_def, path_type_def))
    def test_intersection(self, paths):
        Path(paths[0]).intersection(Path(paths[1]))

    @given(st.tuples(path_type_def, path_type_def))
    def test_difference(self, paths):
        Path(paths[0]).difference(Path(paths[1]))

    @given(st.tuples(path_type_def, st.floats()))
    def test_offset(self, paths):
        Path(paths[0]).offset(paths[1])

    @settings(deadline=DEADLINE)
    @given(st.tuples(shape_type_def, shape_type_def))
    def test_union_shapes(self, paths):
        _path1 = Path(paths[0])
        _path2 = Path(paths[1])
        _path_res = _path1.union(_path2)
        sample_area(_path1, _path2, _path_res, lambda x,y : x or y)

    @settings(deadline=DEADLINE)
    @given(st.tuples(shape_type_def, shape_type_def))
    def test_intersection_shapes(self, paths):
        _path1 = Path(paths[0])
        _path2 = Path(paths[1])
        _path_res = _path1.intersection(_path2)
        sample_area(_path1, _path2, _path_res, lambda x, y: x and y)

    @settings(deadline=DEADLINE)
    @given(st.tuples(shape_type_def, shape_type_def))
    def test_difference_shapes(self, paths):
        _path1 = Path(paths[0])
        _path2 = Path(paths[1])
        _path_res = _path1.difference(_path2)
        sample_area(_path1, _path2, _path_res, lambda x, y: x and not y)
    """

    @settings(deadline=DEADLINE)
    @given(st.tuples(shape_type_def, st.floats()))
    def test_offset_shapes(self, paths):
        _path = Path(paths[0])
        _path_res = _path.offset(paths[1])
        sample_area_offset(_path, paths[1], _path_res)



# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Martin Owens <doctormo@gmail.com>
#                    Sergei Izmailov <sergei.a.izmailov@gmail.com>
#                    Thomas Holder <thomas.holder@schrodinger.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# pylint: disable=arguments-differ
"""
Element interface for patterns, filters, gradients and path effects.
"""

from typing import List, Tuple, TYPE_CHECKING

from lxml import etree
from math import sin, cos

from ..transforms import Transform, BoundingBox, Vector2d
from ..utils import parse_percent

from ..styles import Style
from ..colors import Color
from ._utils import addNS
from ._base import BaseElement


if TYPE_CHECKING:
    from ._svg import SvgDocumentElement


class Filter(BaseElement):
    """A filter (usually in defs)"""

    tag_name = "filter"

    def add_primitive(self, fe_type, **args):
        """Create a filter primitive with the given arguments"""
        elem = etree.SubElement(self, addNS(fe_type, "svg"))
        elem.update(**args)
        return elem

    class Primitive(BaseElement):
        """Any filter primitive"""

    class Blend(Primitive):
        """Blend Filter element"""

        tag_name = "feBlend"

    class ColorMatrix(Primitive):
        """ColorMatrix Filter element"""

        tag_name = "feColorMatrix"

    class ComponentTransfer(Primitive):
        """ComponentTransfer Filter element"""

        tag_name = "feComponentTransfer"

    class Composite(Primitive):
        """Composite Filter element"""

        tag_name = "feComposite"

    class ConvolveMatrix(Primitive):
        """ConvolveMatrix Filter element"""

        tag_name = "feConvolveMatrix"

    class DiffuseLighting(Primitive):
        """DiffuseLightning Filter element"""

        tag_name = "feDiffuseLighting"

    class DisplacementMap(Primitive):
        """Flood Filter element"""

        tag_name = "feDisplacementMap"

    class Flood(Primitive):
        """DiffuseLightning Filter element"""

        tag_name = "feFlood"

    class GaussianBlur(Primitive):
        """GaussianBlur Filter element"""

        tag_name = "feGaussianBlur"

    class Image(Primitive):
        """Image Filter element"""

        tag_name = "feImage"

    class Merge(Primitive):
        """Merge Filter element"""

        tag_name = "feMerge"

    class Morphology(Primitive):
        """Morphology Filter element"""

        tag_name = "feMorphology"

    class Offset(Primitive):
        """Offset Filter element"""

        tag_name = "feOffset"

    class SpecularLighting(Primitive):
        """SpecularLighting Filter element"""

        tag_name = "feSpecularLighting"

    class Tile(Primitive):
        """Tile Filter element"""

        tag_name = "feTile"

    class Turbulence(Primitive):
        """Turbulence Filter element"""

        tag_name = "feTurbulence"


class Stop(BaseElement):
    """Gradient stop

    .. versionadded:: 1.1"""

    tag_name = "stop"

    @property
    def offset(self) -> float:
        """The offset of the gradient stop"""
        return self.get("offset")

    @offset.setter
    def offset(self, number):
        self.set("offset", number)

    def interpolate(self, other, fraction):
        """Interpolate gradient stops"""
        from ..tween import StopInterpolator

        return StopInterpolator(self, other).interpolate(fraction)


class Pattern(BaseElement):
    """Pattern element which is used in the def to control repeating fills"""

    tag_name = "pattern"
    WRAPPED_ATTRS = BaseElement.WRAPPED_ATTRS + (("patternTransform", Transform),)


class Gradient(BaseElement):
    """A gradient instruction usually in the defs."""

    WRAPPED_ATTRS = BaseElement.WRAPPED_ATTRS + (("gradientTransform", Transform),)
    """Additional to the :attr:`~inkex.elements._base.BaseElement.WRAPPED_ATTRS` of 
    :class:`~inkex.elements._base.BaseElement`, ``gradientTransform`` is wrapped."""

    orientation_attributes = ()  # type: Tuple[str, ...]
    """
    .. versionadded:: 1.1
    """

    @property
    def stops(self):
        """Return an ordered list of own or linked stop nodes

        .. versionadded:: 1.1"""
        gradcolor = (
            self.href
            if isinstance(self.href, (LinearGradient, RadialGradient))
            else self
        )
        return sorted(
            [child for child in gradcolor if isinstance(child, Stop)],
            key=lambda x: parse_percent(x.offset),
        )

    @property
    def stop_offsets(self):
        # type: () -> List[float]
        """Return a list of own or linked stop offsets

        .. versionadded:: 1.1"""
        return [child.offset for child in self.stops]

    @property
    def stop_styles(self):  # type: () -> List[Style]
        """Return a list of own or linked offset styles

        .. versionadded:: 1.1"""
        return [child.style for child in self.stops]

    def remove_orientation(self):
        """Remove all orientation attributes from this element

        .. versionadded:: 1.1"""
        for attr in self.orientation_attributes:
            self.pop(attr)

    def interpolate(
        self, other, fraction, svg=None
    ):  # type: (LinearGradient, float, SvgDocumentElement) -> LinearGradient
        """Interpolate with another gradient.

        .. versionadded:: 1.1"""
        from ..tween import GradientInterpolator

        return GradientInterpolator(self, other, svg).interpolate(fraction)

    def stops_and_orientation(self):
        """Return a copy of all the stops in this gradient

        .. versionadded:: 1.1"""
        stops = self.copy()
        stops.remove_orientation()
        orientation = self.copy()
        orientation.remove_all(Stop)
        return stops, orientation


class LinearGradient(Gradient):
    """LinearGradient element"""

    tag_name = "linearGradient"
    orientation_attributes = ("x1", "y1", "x2", "y2")
    """
    .. versionadded:: 1.1
    """

    def apply_transform(self):  # type: () -> None
        """Apply transform to orientation points and set it to identity.
        This function is dangerous if userSpaceOnUse is set in the gradient
        .. versionadded:: 1.1
        """
        trans = self.pop("gradientTransform")
        pt1 = (
            self.to_dimensionless(self.get("x1")),
            self.to_dimensionless(self.get("y1")),
        )
        pt2 = (
            self.to_dimensionless(self.get("x2")),
            self.to_dimensionless(self.get("y2")),
        )
        p1t = trans.apply_to_point(pt1)
        p2t = trans.apply_to_point(pt2)
        self.update(
            x1=self.to_dimensionless(p1t[0]),
            y1=self.to_dimensionless(p1t[1]),
            x2=self.to_dimensionless(p2t[0]),
            y2=self.to_dimensionless(p2t[1]),
        )

    def sample_color(self, bbox: BoundingBox, point: Vector2d, debug=False): # type: () -> Color
        """ given a BoundingBox and a point, what color would the point render as?
        """
        transform = Transform()
        x1 = float(self.get("x1", 0))
        y1 = float(self.get("y1", 0))
        x2 = float(self.get("x2", 1))
        y2 = float(self.get("y2", 0))
        transform.add_translate(x1, y1)
        coor_vec = Vector2d(x2-x1, y2-y1)
        #transform.add_rotate(coor_vec.angle, (x1, y1)) # todo: determine center
        #transform.add_scale(coor_vec.x, coor_vec.y)

        if self.get("gradientUnits") == "userSpaceOnUse":
            grad_transform = Transform(self.get("gradientTransform"))
            if debug:
                print(f"setting userspace transform: {grad_transform} {transform}")
            transform = grad_transform*transform
            #coor_vec = transform*coor_vec
            #transform.add_scale(coor_vec.length)
            if debug:
                print(f"userspace resulting transform: {transform}")
        else: # default to objectBoundingBox
            # if bounding box,  scale by the bounding box
            point.x -= bbox.x.minimum
            point.y -= bbox.y.minimum
            bbox_transform = Transform()
            bbox_transform.add_scale(bbox.width, bbox.height)
            if debug:
                print(f"setting bbox transform: {bbox_transform}")

            transform @= bbox_transform
        # project the target point into a rectilinear space and determine which offsets it falls between
        grad_transformed = transform.apply_to_point(coor_vec)
        angle_diff = grad_transformed.angle-point.angle if point.angle else grad_transformed.angle
        h_prime = point.length*cos(angle_diff)
        projection_y = h_prime * sin(grad_transformed.angle)
        projection_x = h_prime * cos(grad_transformed.angle)
        try:
            proj_frac = h_prime/grad_transformed.length
        except ZeroDivisionError as err:
            raise ValueError(f"can't invert:{grad_transformed}")
        if debug:
            print(f"proj calc: grad_vector {grad_transformed} {coor_vec} angle_diff {angle_diff} = {grad_transformed.angle} - {point.angle} point "
                  f"length {point.length} h_prime: {h_prime} grad_vector: {grad_transformed.length} proj_frac: {proj_frac}")
        lower_i = None
        upper_i = None
        found_limits = False
        for i in range(len(self.stop_offsets)-1):
            if proj_frac <= float(self.stop_offsets[i]):
                lower_i = None
                upper_i = i
                found_limits = True
                break
            elif float(self.stop_offsets[i]) < proj_frac <= float(self.stop_offsets[i+1]):
                lower_i = i
                upper_i = i + 1
                found_limits = True
                break
        if not found_limits:
            lower_i = len(self.stop_offsets)-1
            upper_i = None
        if lower_i is None:
            return [(1, self.stops[upper_i])]
        if upper_i is None:
            return [(1, self.stops[lower_i])]
        upper_offset = float(self.stops[upper_i].offset)
        lower_offset = float(self.stops[lower_i].offset)
        space_between_stops = upper_offset-lower_offset
        diff1 = (proj_frac-lower_offset)/space_between_stops
        diff2 = (upper_offset-proj_frac)/space_between_stops
        return [(diff1, self.stops[lower_i]), (diff2, self.stops[upper_i])]






class RadialGradient(Gradient):
    """RadialGradient element"""

    tag_name = "radialGradient"
    orientation_attributes = ("cx", "cy", "fx", "fy", "r")
    """
    .. versionadded:: 1.1
    """

    def apply_transform(self):  # type: () -> None
        """Apply transform to orientation points and set it to identity.

        .. versionadded:: 1.1
        """
        trans = self.pop("gradientTransform")
        pt1 = (
            self.to_dimensionless(self.get("cx")),
            self.to_dimensionless(self.get("cy")),
        )
        pt2 = (
            self.to_dimensionless(self.get("fx")),
            self.to_dimensionless(self.get("fy")),
        )
        p1t = trans.apply_to_point(pt1)
        p2t = trans.apply_to_point(pt2)
        self.update(
            cx=self.to_dimensionless(p1t[0]),
            cy=self.to_dimensionless(p1t[1]),
            fx=self.to_dimensionless(p2t[0]),
            fy=self.to_dimensionless(p2t[1]),
        )


class PathEffect(BaseElement):
    """Inkscape LPE element"""

    tag_name = "inkscape:path-effect"


class MeshGradient(Gradient):
    """Usable MeshGradient XML base class

    .. versionadded:: 1.1"""

    tag_name = "meshgradient"

    @classmethod
    def new_mesh(cls, pos=None, rows=1, cols=1, autocollect=True):
        """Return skeleton of 1x1 meshgradient definition."""
        # initial point
        if pos is None or len(pos) != 2:
            pos = [0.0, 0.0]
        # create nested elements for rows x cols mesh
        meshgradient = cls()
        for _ in range(rows):
            meshrow: BaseElement = meshgradient.add(MeshRow())
            for _ in range(cols):
                meshrow.append(MeshPatch())
        # set meshgradient attributes
        meshgradient.set("gradientUnits", "userSpaceOnUse")
        meshgradient.set("x", pos[0])
        meshgradient.set("y", pos[1])
        if autocollect:
            meshgradient.set("inkscape:collect", "always")
        return meshgradient


class MeshRow(BaseElement):
    """Each row of a mesh gradient

    .. versionadded:: 1.1"""

    tag_name = "meshrow"


class MeshPatch(BaseElement):
    """Each column or 'patch' in a mesh gradient

    .. versionadded:: 1.1"""

    tag_name = "meshpatch"

    def stops(self, edges, colors):
        """Add or edit meshpatch stops with path and stop-color."""
        # iterate stops based on number of edges (path data)
        for i, edge in enumerate(edges):
            if i < len(self):
                stop = self[i]
            else:
                stop = self.add(Stop())

            # set edge path data
            stop.set("path", str(edge))
            # set stop color
            stop.style["stop-color"] = str(colors[i % 2])

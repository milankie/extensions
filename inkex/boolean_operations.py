from collections import defaultdict
from svgpathtools.path import Path as PTPath, Line as PTLine, Arc as PTArc, CubicBezier as PTCubicBezier, QuadraticBezier as PTQuadraticBezier
from typing import Union, Dict, List

TOLERANCE = 0.2


def remove_degenerate_segments(input_path: PTPath) -> PTPath:
    segments = []
    for segment in input_path:
        if segment.length() == 0:
            continue
        segments.append(segment)
    return PTPath(*segments)


def chain_graph(graph: Dict[int, List[int]], edges: List[PTPath]) -> List[PTPath]:
    chained_path = []
    edges_to_visit = [key for key in graph if graph[key]]
    edges_to_visit.sort(key=lambda x: abs(edges[x].start))
    edges_visited = []
    current_edge = None
    total_operations = 0
    while edges_to_visit:
        if not current_edge:
            current_edge = edges_to_visit.pop()
        if current_edge in edges_visited:
            current_edge = None
            continue
        if total_operations > 100:
            raise ValueError("total number of operations exceeded!")
            break
        chained_path.append(current_edge)

        edges_visited.append(current_edge)
        branches = [
            branch for branch in graph[current_edge] if branch not in edges_visited
        ]
        if branches:
            current_edge = branches[0]
        else:
            current_edge = None
    if not chained_path:
        raise ValueError(f"chained_path is empty! graph was: {graph.get(current_edge)} {graph}")
    return combine_segments([edges[i] for i in chained_path])


def segment_is_inside(
    container: PTPath,
    segment: Union[PTPath, PTLine, float],
    debug=False,
    tolerance=0.01,
):

    # this assumes that there are no intersections between the segment and its surrounding container
    try:
        xmin_con, xmax_con, ymin_con, ymax_con = container.bbox()
        if debug:
            print(f"{container.bbox()=}")
    except ValueError:
        raise Exception(f"could not get bbox out of {container}")

    if isinstance(segment, complex):
        if debug:
            print(f"segment was a point inside the container")
        return is_inside(container, segment, debug=debug, tolerance=tolerance)
    if isinstance(segment, PTPath):
        assert (
            segment._segments
        )  # the path segment shouldn't be empty, this is indeterminate
    xmin_seg, xmax_seg, ymin_seg, ymax_seg = segment.bbox()
    bbox_same = (
        xmin_con == xmin_seg
        and xmax_con == xmax_seg
        and ymin_seg == ymin_con
        and ymax_seg == ymax_con
    )
    if bbox_same:
        if debug:
            print(
                "the bbox for the segment and container are completely the same, evaluating the midpoint"
            )
        _point = segment.point(0.5)
        if _point is None:
            raise ValueError(f"failed to get the midpoint of {segment}")
        return is_inside(
            container, _point, debug=debug, tolerance=tolerance
        )
    bbox_inside = (xmin_con < xmin_seg < xmax_seg < xmax_con) and (
        ymin_con < ymin_seg < ymax_seg < ymax_con
    )
    if bbox_inside:  # the bboxes don't completely overlap, therefore it's outside
        if debug:
            print(
                f"the bbox for the segment is completely inside the container {xmin_con, xmax_con, ymin_con, ymax_con} segment {xmin_seg, xmax_seg, ymin_seg, ymax_seg}"
            )
        return True
    # next we have to actually check whether the segment is inside. Take the midpoint of the segment.
    # Since we know there shouldn't be any intersections, we only have to sample once
    mid_point = segment.point(0.5)
    if mid_point is None:
        raise ValueError(f"unable to get midpoint of {segment}")
    return is_inside(container, mid_point, debug=debug, tolerance=tolerance)


def is_inside(container, point, debug=False, tolerance=0.01):
    """
    tripartite logic:
    True - point is definitely inside
    False - point is definitely outside
    None - point is within the tolerance of the edge
    """
    xmin, xmax, ymin, ymax = container.bbox()
    # if the point is on the edge of the bbox, assume it's outside
    diffs = [
        abs(point.real - xmin) < tolerance * abs(xmax - xmin),
        abs(point.real - xmax) < tolerance * abs(xmax - xmin),
        abs(point.imag - ymin) < tolerance * abs(ymax - ymin),
        abs(point.imag - ymax) < tolerance * abs(ymax - ymin),
    ]

    if any(diffs):
        if debug:
            print(f"{point=} is on bbox, {diffs=} {tolerance*abs(xmax-xmin)=}")
        return None

    if point.real < xmin:
        if debug:
            print("to the left of the bbox")
        return False
    if point.real > xmax:
        if debug:
            print("to the right of the bbox")
        return False
    if point.imag < ymin:
        if debug:
            print("below the bbox")
        return False
    if point.imag > ymax:
        if debug:
            print("above the bbox")
        return False

    # make sure the lines are actually out of the bbox by adding a shrinking/enlarging factor
    span_line_upper = PTLine(0.9 * (xmin + ymin * 1j), point)
    span_line_lower = PTLine(point, 1.1 * (xmax + ymax * 1j))
    upper_intersections = PTPath(span_line_upper).intersect(container)
    lower_intersections = PTPath(span_line_lower).intersect(container)
    if debug:
        print(f"container {container=}")
        print(
            f"num intersecions {len(upper_intersections)=} {len(lower_intersections)=} "
        )
        print(
            f"is_inside debug {point=} {xmin+ymin*1j} {xmax+ymax*1j} {upper_intersections=} {lower_intersections=} "
        )

    return len(upper_intersections) % 2 or len(lower_intersections) % 2


def find_orientation(in_path):
    points = []
    for segment in in_path:
        points.append(segment.end)
    # https://en.wikipedia.org/wiki/Curve_orientation#Orientation_of_a_simple_polygon
    orientations = 0

    for i in range(len(points)):
        _piece = []
        for j in range(3):
            _piece.append(points[(i + j) % len(points)])
        # x2*y3+x1*y2+y1*x3 - (y1*x2+y2*x3+x1*y3)
        orientations += (
            _piece[1].real * _piece[2].imag
            + _piece[0].real * _piece[1].imag
            + _piece[0].imag * _piece[2].real
            - (
                _piece[0].imag * _piece[1].real
                + _piece[1].imag * _piece[2].real
                + _piece[0].real * _piece[2].imag
            )
        )
    return orientations > 0


def intersect_over_all(path1, path2):
    all_intersections = []
    for i, segment in enumerate(path2):
        current_intersections = path1.intersect(segment)
        all_intersections += [(t1, t2, i) for (t1, t2) in current_intersections]
    return all_intersections


def combine_segments(segments):
    # svgpathtools does not dump "Path" segments when generating d strings
    output_path = PTPath()
    previous_end = None
    for segment in segments:
        if isinstance(segment, PTPath):
            for path_segment in segment._segments:
                if previous_end and abs(path_segment.start - previous_end) < TOLERANCE:
                    path_segment.start = previous_end
                output_path.insert(len(output_path._segments), path_segment)
                previous_end = path_segment.end
        elif isinstance(segment, PTLine) or isinstance(segment, PTArc) or isinstance(segment, PTCubicBezier) \
                or isinstance(segment, PTQuadraticBezier):
            if previous_end and abs(segment.start - previous_end) < TOLERANCE:
                segment.start = previous_end
            output_path.insert(len(output_path._segments), segment)
            previous_end = segment.end
        else:
            raise TypeError(
                f"segment is of incompatible type: {type(segment)} {segments}"
            )
    return output_path


class BooleanOperator:
    """
    Class for doing boolean calculations
    """

    def __init__(self, path1: PTPath, path2: PTPath, debug=False):
        self.path_pt = [None, None]
        self.debug = debug
        self.path_pt[0] = remove_degenerate_segments(path1)  # .to_svgpathtools()
        self.path_pt[1] = remove_degenerate_segments(path2)  # .to_svgpathtools()
        # self.nodes is a list of the coordinates of the nodes in the graph
        # self.graph is a dict of dicts of svgpathtool segments
        self.graph: Dict[int, List[int]] = dict()

        self.edges: List[PTPath] = []
        self.build_intersection_graph()

    def build_intersection_graph(self, tolerance=0.01):
        # svgpathtools fails to find intersections over the entire path if any of the individual paths are the same.
        # remove segments in path1 that are in path0 before building intersection graph
        _deduped_path = []
        segments0 = [segment0 for segment0 in self.path_pt[0]]
        for segment1 in self.path_pt[1]:
            if segment1 not in segments0:
                _deduped_path.append(segment1)
        _deduped_path = PTPath(*_deduped_path)
        # make a graph of all intersection points between this path and another
        # (T1, seg1, t1), (T2, seg2, t2)

        try:
            # what should we do if there are segments exactly in common between these?
            # we need to do pairwise comparison between the two segments t2T(seg, t)
            intersections = []
            for i, segment_i in enumerate(self.path_pt[0]):
                for j, segment_j in enumerate(_deduped_path):
                    if segment_i == segment_j:
                        continue
                    seg_intersections = segment_i.intersect(segment_j)
                    for seg_intersection in seg_intersections:
                        t1 = seg_intersection[0]
                        t2 = seg_intersection[1]
                        T1 = self.path_pt[0].t2T(i, t1)
                        T2 = self.path_pt[1].t2T(j, t2)
                        intersections.append(((T1, i, t1), (T2, j, t2)))
            for intersection in intersections:
                assert 0 <= intersection[0][0] <= 1, f"got t value {intersection[0]} outside of bounds with {self.path_pt[0]} {_deduped_path}"
                assert 0 <= intersection[1][0] <= 1, f"got t value {intersection[0]} outside of bounds with {self.path_pt[0]} {_deduped_path}"

        except AssertionError as err:
            if self.path_pt[0] == self.path_pt[1]:
                # the graphs are identical, so it is as if there is no graph
                self.edges = self.path_pt
                return

            path0 = self.path_pt[0].d()
            path1 = self.path_pt[1].d()
            raise AssertionError(
                f"Could not build intersections between {path0=} and {path1=}: err {err=}"
            )
        if self.debug:
            print(f"all intersections")
            for intersection in intersections:
                print(intersection)
        if not intersections:
            self.edges = self.path_pt
            return
        endpoints = defaultdict(dict)
        # add the first point to the end of the intersections so that we can loop back around
        for i in [0, 1]:
            pt_ts = sorted([intersection[i][0] for intersection in intersections])

            pt_ts.append(pt_ts[0])
            start_ts = pt_ts.pop(0)
            while pt_ts:
                end_ts = pt_ts.pop(0)
                if end_ts < start_ts or end_ts > 1.0:
                    end_ts = 1
                if start_ts == end_ts:
                    continue
                assert start_ts < end_ts, f"start {start_ts} is larger than end {end_ts}"
                segment = self.path_pt[i].cropped(start_ts, end_ts)

                if self.debug:
                    print(
                        f"graph {i} {len(self.edges)} start ts {start_ts} end ts {end_ts} "
                    )
                    print(segment.d(), self.path_pt[i].d())
                # confirm that the reversed segment hasn't already been added
                if segment.reversed() in self.edges:
                    """
                    print(
                        f"skipping what would have been {len(self.edges)} because {segment.reversed()} is already in {self.edges}"
                    )
                    """
                    start_ts = end_ts
                    continue
                # else:
                #    print(
                #        f"{segment.reversed()} not in {self.edges}")
                self.edges.append(segment)
                if start_ts not in endpoints[i]:
                    endpoints[i][start_ts] = []
                if end_ts not in endpoints[i]:
                    endpoints[i][end_ts] = []
                endpoints[i][start_ts].append((end_ts, len(self.edges) - 1))
                endpoints[i][end_ts].append((start_ts, len(self.edges) - 1))
                start_ts = end_ts
        for intersection in intersections:
            # edges that touch this intersection
            t1 = intersection[0][0]
            t2 = intersection[1][0]
            _edges = [endpoint[1] for endpoint in endpoints[0].get(t1, [])] + [
                endpoint[1] for endpoint in endpoints[1].get(t2, [])
            ]
            for edge_i in _edges:
                for edge_j in _edges:
                    if edge_i == edge_j:
                        continue
                    if edge_j not in self.graph.get(edge_i, []):
                        if edge_i not in self.graph:
                            self.graph[edge_i] = []
                        self.graph[edge_i].append(edge_j)

    def is_inside(self, edge: PTPath, path_index=0, debug=False):
        # is the edge inside the path at path_index?

        return segment_is_inside(self.path_pt[path_index], edge, debug=debug)

    def chain_graph(self):
        return chain_graph(self.graph, self.edges)

    def prune(self, condition):
        nodes = list(self.graph.keys())
        for node in nodes:
            if (
                node not in self.graph
            ):  # the node may have been already removed through the pruning process
                continue
            childnodes = self.graph[node]
            for childnode in childnodes:
                edge = self.edges[childnode]
                if condition(edge):
                    for node_sub in nodes:
                        if node_sub not in self.graph:
                            continue
                        if childnode in self.graph[node_sub]:
                            self.graph[node_sub].remove(childnode)
                            if not self.graph[node_sub]:
                                del self.graph[node_sub]

                    if childnode in self.graph:
                        del self.graph[childnode]

    def union(self):
        # prune the edges that are "inside"
        self.prune(lambda x: self.is_inside(x, 0) or self.is_inside(x, 1))
        # if there are no intersections, just append the paths together
        if not self.graph:
            # if one path is completely inside the other, we need to kill the inner path.
            if len(self.path_pt[0]) == 0:
                return self.path_pt[1]
            elif len(self.path_pt[1]) == 0:
                return self.path_pt[0]
            elif self.is_inside(self.path_pt[1].start, 0):
                return self.path_pt[0]
            elif self.is_inside(self.path_pt[0].start, 1):
                return self.path_pt[1]
            else:  # just return the combination of both
                return combine_segments(
                    [segment for segment in self.path_pt[0]]
                    + [segment for segment in self.path_pt[1]]
                )

        # find a path through all edges
        return self.chain_graph()

    def intersection_should_prune(self, segment):
        if self.is_inside(segment, 0) or self.is_inside(segment, 1):
            return False
        elif self.is_inside(segment, 1) == False or self.is_inside(segment, 0) == False:
            # it is definitely outside one of the shapes, prune it
            return True
        else:  # it may be on the edge of both, leave it in
            return False

    def intersection(self, debug=False):
        before_keys = list(self.graph.keys())
        if debug:
            print(f"intersection graph before pruning: {self.graph}")
        # prune the edges that are not inside

        self.prune(self.intersection_should_prune)
        # find a path through all edges
        after_keys = self.graph.keys()

        if debug:
            print(
                f"intersection graph after pruning: {self.graph} {before_keys} {after_keys}"
            )
        #if before_keys and not after_keys:
        #    raise ValueError(
        #        f"pruned entire graph away! paths were {self.path_pt[0].d()} and {self.path_pt[1].d()}"
        #    )
        if not self.graph:
            if debug:
                print(
                    f"no graph, inside values {self.is_inside(self.path_pt[1], 0, debug)} "
                    f"{self.is_inside(self.path_pt[0], 1, debug)}"
                )
            if len(self.path_pt[0]) == 0 or len(self.path_pt[1]) == 0:
                return PTPath()
            # if there are no intersections, if p1 is inside p2 return p1, if p2 is inside p1 return p2,
            # else return the empty set
            if (
                self.is_inside(self.path_pt[1], 0)
                or self.is_inside(self.path_pt[1], 0) is None
            ):
                return self.path_pt[1]
            elif (
                self.is_inside(self.path_pt[0], 1)
                or self.is_inside(self.path_pt[0], 1) is None
            ):
                return self.path_pt[0]
            else:
                return PTPath()

        return self.chain_graph()

    def difference(self):
        self.prune(lambda x: self.is_inside(x, 1))
        # prune the edges that inside path2
        if (
            not self.graph
        ):
            # there are no intersections, and one path is inside the other, the solution is the smaller shape cut out of the larger shape.
            if len(self.path_pt[1]) == 0 or len(self.path_pt[0]) == 0: # if the thing you're subtracting is empty, or the original path is empty, return the original path
                return self.path_pt[0]

            if self.is_inside(self.path_pt[0].start, 1):
                return combine_segments(
                    [segment for segment in self.path_pt[1]]
                    + [segment for segment in self.path_pt[0].reversed()]
                )
            elif self.is_inside(self.path_pt[1].start, 0):
                return combine_segments(
                    [segment for segment in self.path_pt[0]]
                    + [segment for segment in self.path_pt[1].reversed()]
                )
            else:
                return PTPath()

        # find a path through all edges
        return self.chain_graph()

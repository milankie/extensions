from inkex.paths import Quadratic, line, Line, quadratic, curve, Curve, move, Move, zoneClose, ZoneClose, horz, \
    Horz, vert, Vert, Arc, arc, Path
from svgpathtools.path import QuadraticBezier as PTQuadraticBezier, \
    CubicBezier as PTCubicBezier, Arc as PTArc
from shapely.geometry import Polygon, Point
GRID_SIZE = 100


def polygon_to_path(_polygon):
    xx, yy = _polygon.exterior.coords.xy

    # Note above return values are of type `array.array`
    points = list(zip(xx.tolist(), yy.tolist()))
    _path = Path()
    _path.append(Move(*points[-1]))
    for _point in points:
        _path.append(Line(*_point))
    _path.append(zoneClose)
    return _path


def interpolate_points(seg, converted_seg):
    _polygon_points = []
    _polygon_points.append([seg.previous_end_point.x, seg.previous_end_point.y])

    for i in range(0, 20):
        midpoint = converted_seg.point(i/20.0)
        _polygon_points.append([midpoint.real, midpoint.imag])
    _polygon_points.append([seg.end_point.x, seg.end_point.y])
    return _polygon_points


def path_to_polygon(_path):
    _polygon_points = []

    for seg in _path.proxy_iterator():
        converted_seg = None
        if isinstance(seg.command, Line) or isinstance(seg.command, line) or isinstance(seg.command, horz) \
                or isinstance(seg.command, vert) or isinstance(seg.command, Horz) or isinstance(seg.command, Vert):
            _polygon_points.append([seg.previous_end_point.x , seg.previous_end_point.y ])
            _polygon_points.append([seg.end_point.x, seg.end_point.y])
        elif isinstance(seg.command, Arc) or isinstance(seg.command, arc):
            if seg.previous_end_point.x + seg.previous_end_point.y * 1j == seg.end_point.x + seg.end_point.y * 1j:
                continue
            elif seg.command.ry == 0 or seg.command.rx == 0:
                _polygon_points.append([seg.previous_end_point.x, seg.previous_end_point.y])
                _polygon_points.append([seg.end_point.x, seg.end_point.y])
            else:
                converted_seg = PTArc(start=seg.previous_end_point.x + seg.previous_end_point.y * 1j,
                                      radius=seg.command.rx + seg.command.ry * 1j, large_arc=seg.command.large_arc,
                                      rotation=seg.command.x_axis_rotation,
                                      sweep=seg.command.sweep, end=seg.end_point.x + seg.end_point.y * 1j)
                _polygon_points += interpolate_points(seg, converted_seg)
        elif isinstance(seg.command, Quadratic) or isinstance(seg.command, quadratic):
            if isinstance(seg.command, quadratic):
                seg.command = seg.to_absolute()
            converted_seg = PTQuadraticBezier(start=seg.previous_end_point.x + seg.previous_end_point.y * 1j,
                                              control=seg.command.x2 + seg.command.y2 * 1j,
                                              end=seg.end_point.x + seg.end_point.y * 1j)
            _polygon_points += interpolate_points(seg, converted_seg)
        elif isinstance(seg.command, Curve) or isinstance(seg.command, curve):
            if isinstance(seg.command, curve):
                seg.command = seg.to_absolute()
            converted_seg = PTCubicBezier(start=seg.previous_end_point.x + seg.previous_end_point.y * 1j,
                                          control1=seg.command.x2 + seg.command.y2 * 1j,
                                          control2=seg.command.x3 + seg.command.y3 * 1j,
                                          end=seg.end_point.x + seg.end_point.y * 1j)
            _polygon_points += interpolate_points(seg, converted_seg)
        elif isinstance(seg.command, Move) or isinstance(seg.command, move):
            continue
        elif isinstance(seg.command, zoneClose) or isinstance(seg.command, ZoneClose):
            _polygon_points.append([seg.previous_end_point.x, seg.previous_end_point.y])
            _polygon_points.append([seg.end_point.x, seg.end_point.y])
        else:
            raise TypeError(f"not sure what to do with {seg}")

    poly = Polygon(_polygon_points)
    # scale tolerance by the size of the shape
    bounding = _path.bounding_box()
    _diag = bounding.size.length
    _output = poly.simplify(tolerance=_diag/500.0)
    return _output


def sample_area(_path1, _path2, _path_res, condition, grid_size=GRID_SIZE):
    poly1 = path_to_polygon(_path1)
    poly2 = path_to_polygon(_path2)
    poly_res = path_to_polygon(_path_res)
    for x in range(-GRID_SIZE, GRID_SIZE):
        for y in range(-GRID_SIZE, GRID_SIZE):
            test_point = Point(x, y)
            if poly1.exterior.contains(test_point) or poly2.exterior.contains(test_point):
                continue
            assert condition(poly1.contains(test_point), poly2.contains(test_point)) == poly_res.contains(test_point), \
                f"polygons are not as expected {test_point} \n {poly_res} \n {poly1} \n {poly2}"


def sample_area_offset(_path, offset, _path_res):
    poly1 = path_to_polygon(_path).buffer(offset)
    poly_res = path_to_polygon(_path_res)
    _bounding_box = _path.bounding_box()
    num_points = 100
    good_points = 0
    all_points = 0
    for i in range(0, num_points):
        for j in range(0, num_points):
            x = i*_bounding_box.width/num_points + _bounding_box.left
            y = j*_bounding_box.height/num_points + _bounding_box.top
            test_point = Point(x, y)
            if poly1.contains(test_point):
                all_points += 1
                if poly_res.contains(test_point):
                    good_points += 1

    print(f"number of good points {good_points} all points {all_points}")
    assert good_points/all_points > 0.9, f"{polygon_to_path(poly1)}\n does not overlap enough with {_path_res}"